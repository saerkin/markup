# -----------------------------------------------------------------------
# Platform.
# -----------------------------------------------------------------------
# Determine the platform
if(WIN32)
  set(PLATFORM_NAME "win")
elseif(UNIX)
  set(PLATFORM_NAME "linux")
else()
  message(FATAL_ERROR "Unsupported platform.")
endif()

# Determine platform bits.
if(CMAKE_SIZEOF_VOID_P EQUAL 4)
  set(PLATFORM_BITNESS "32")
elseif(CMAKE_SIZEOF_VOID_P EQUAL 8)
  set(PLATFORM_BITNESS "64")
endif()

# Set PLATFORM.
set(PLATFORM "${PLATFORM_NAME}${PLATFORM_BITNESS}")

message("Platform is ${PLATFORM}")

# -----------------------------------------------------------------------
