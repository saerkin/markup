if (MSVC)
  # -MDd = Debug Multithreaded DLL
  # -GR = Enable Run-Time Type Information
  # -Gm = minimal rebuild
  # -ZI = debug&continue
  # -Od = Disable any optimization
  set(CMAKE_CXX_FLAGS_DEBUG "/MDd /GR /Gm /ZI /Od")

  # -MD = Multithreaded DLL
  # -GR = Enable Run-Time Type Information
  # -Gy = function level linking
  # -Ob2 = In-line Function Expansion is Any Suitable
  # -Oi = Generate Intrinsic Functions
  # -Ot = Favor Fast Code
  # -Ox = Full Optimization
  # -Oy = Frame-Pointer Omission
  set(CMAKE_CXX_FLAGS_RELEASE "/MD /GR /Gy /Ob2 /Oi /Ot /Ox /Oy")
endif (MSVC)

if (MSVC60)

  # -Gi = incremental compilation
  # -GX = Enable Exception Handling
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /GX /Gi")

  # -Gf = Eliminate Duplicate Strings
  # -GX = Enable Exception Handling
  # -Og = global optimization
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /GX /Og /Gf")

elseif (MSVC80 OR MSVC90 OR MSVC10 OR MSVC11 OR MSVC12)

  add_definitions(-D_CRT_SECURE_NO_WARNINGS -D_CRT_SECURE_NO_DEPRECATE)

  # /EHsc  = Enable Exception Handling
  # /RTCsu = Basic Runtime Checks is Both
  set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /EHsc /RTCsu")

  # /EHsc  = Enable Exception Handling
  # /GL    = Whole Program Optimization
  set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /EHsc /GL")

endif ()

# -----------------------------------------------------------------------------
