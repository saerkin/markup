# noinspection PyUnresolvedReferences
from conans import ConanFile, CMake


class MarkupConan(ConanFile):
  name = "markup"
  version = "0.8.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/markup.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  generators = "cmake"
  requires = \
    "axiom-minmax/1.0@sergey-erkin/alpha", \
    "axiom-string/0.1.0@sergey-erkin/experimental", \
    "axiom-collection/0.1.0@sergey-erkin/experimental", \
    "axiom-lifescope/1.0@sergey-erkin/alpha", \

  def build(self):
    cmake = CMake(self)
    cmake.configure(source_folder="")
    cmake.build()

  def package(self):
    self.copy("*.h", dst="include", src="include")
    self.copy("*.lib", dst="lib", keep_path=False)
    self.copy("*.dll", dst="bin", keep_path=False)
    self.copy("*.dylib*", dst="lib", keep_path=False)
    self.copy("*.so", dst="lib", keep_path=False)
    self.copy("*.a", dst="lib", keep_path=False)

  def package_info(self):
    self.cpp_info.includedirs = ["include"]
    self.cpp_info.srcdirs = ["src"]
