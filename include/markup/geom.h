#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-non-private-member-variables-in-classes"
#pragma once
#include <axiom/array.h>
#include <axiom/minmax.h>
#include <axiom/shared_ptr.h>

namespace markup {

class Point;
class Box;
class Polygon;

/**
 * 
 */
class Point {
public:
  double x;
  double y;

// Constructors/destructor
public:
  inline Point()
    : x(-1), y(-1) {}

  inline Point(const double& x, const double& y)
    : x(x), y(y) {}

  inline Point(const Point& other)
    : x(other.x), y(other.y) {}

// Operators
public:
  inline Point& operator=(const Point& other) {
    if (this != &other) {
      x = other.x;
      y = other.y;
    }
    return *this;
  }

  inline bool operator==(const Point& other) const {
    return
      x == other.x
        && y == other.y;
  }

// Methods
public:
  inline Point move(const double& dx, const double& dy) const {
    return Point(
      x + dx,
      y + dy
    );
  }
};

/**
 * 
 */
class Box {
public:
  double top;
  double left;
  double right;
  double bottom;

  static const Box ZERO;

// Constructors
public:
  inline Box() :
    top(-1),
    left(-1),
    right(-1),
    bottom(-1) {}

  inline Box(const double& t, const double& l, const double& r, const double& b) :
    top(t),
    left(l),
    right(r),
    bottom(b) {}

  inline Box(const Point& topLeft, const Point& rightBottom) :
    top(topLeft.y),
    left(topLeft.x),
    right(rightBottom.x),
    bottom(rightBottom.y) {}

  inline Box(const Box& rect) :
    top(rect.top),
    left(rect.left),
    right(rect.right),
    bottom(rect.bottom) {}

  static inline Box fromPoint(const Point& point, const double& width, const double& height) {
    return Box(point, point).extend(width / 2, height / 2);
  }

// Operators
public:
  inline Box& operator=(const Box& other) {
    if (this != &other) {
      top = other.top;
      left = other.left;
      right = other.right;
      bottom = other.bottom;
    }
    return *this;
  }

  inline bool operator==(const Box& box) const {
    return
      this->left == box.left
        && this->top == box.top
        && this->right == box.right
        && this->bottom == box.bottom;
  }

// Methods
public:
  inline double width() const {
    return right - left;
  }

  inline double height() const {
    return bottom - top;
  }

  inline bool isEmpty() const {
    return left == right || top == bottom;
  }

  inline Point topLeft() const {
    return Point(left, top);
  }

  inline Point topRight() const {
    return Point(right, top);
  }

  inline Point bottomRight() const {
    return Point(right, bottom);
  }

  inline Point bottomLeft() const {
    return Point(left, bottom);
  }

  inline Point leftCenter() const {
    return Point(left, (top + bottom) / 2);
  }

  inline Point topCenter() const {
    return Point((left + right) / 2, top);
  }

  inline Point rightCenter() const {
    return Point(right, (top + bottom) / 2);
  }

  inline Point bottomCenter() const {
    return Point((left + right) / 2, bottom);
  }

  inline bool contains(const Point& point) const {
    return
      (left <= point.x && point.x <= right)
        && (top <= point.y && point.y <= bottom);
  }

  inline Box move(const double& dx, const double& dy) const {
    return Box(
      top + dy,
      left + dx,
      right + dx,
      bottom + dy
    );
  }

  inline Box extend(const double& h, const double& v) const {
    return Box(
      top - v,
      left - h,
      right + h,
      bottom + v
    );
  }

  inline Box normalize() const {
    using namespace axiom;
    return Box(
      min(top, bottom),
      min(left, right),
      max(left, right),
      max(top, bottom)
    );
  }
};

inline Box unite(const Box& box1, const Box& box2) {
  using namespace axiom;
  return Box(
    min(box1.top, box2.top),
    min(box1.left, box2.left),
    max(box1.right, box2.right),
    max(box1.bottom, box2.bottom)
  );
}

inline Box intersect(const Box& box1, const Box& box2) {
  using namespace axiom;
  return Box(
    min(box1.top, box2.top),
    min(box1.left, box2.left),
    max(box1.right, box2.right),
    max(box1.bottom, box2.bottom)
  );
}

typedef Box Rectangle;

/**
 *
 */
class Polygon {
public:
  axiom::array<Point> points;

// Constructors
public:
  inline Polygon() {}

  inline explicit Polygon(const axiom::array<Point>& points)
    : points(points) {}

  inline Polygon(const Polygon& polygon)
    : points(polygon.points) {}

// Operators
public:
  inline bool operator==(const Polygon& o) const {
    return this->points == o.points;
  }

  inline bool operator!=(const Polygon& o) const {
    return this->points != o.points;
  }

// Methods
public:
  inline Polygon add(const Point& point) const {
    return Polygon(points.add(point));
  }

  inline Polygon move(double dx, double dy) const {
    using namespace axiom;
    array<Point> points;
    for (iterator<Point> it = this->points.begin(); it.hasNext();) {
      points = points.add(it.next().move(dx, dy));
    }
    return Polygon(points);
  }
};

/**
 *
 */
class Transform {
// Fields
public:
  Point offset;

// Constructors/destructor
public:
  inline Transform() {}

public:
  inline Transform(const Transform& tx)
    : offset(tx.offset) {}

// Operators
private:
  Transform& operator=(const Transform& tx);
};

template<typename T>
T operator*(const Transform& tx, const T& object);

template<typename T>
inline T operator*(const T& object, const Transform& tx) {
  return tx * object;
}

template<>
inline Point operator*(const Transform& tx, const Point& point) {
  return Point(point.x + tx.offset.x, point.y + tx.offset.y);
}

} // end of markup

#pragma clang diagnostic pop