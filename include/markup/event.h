#pragma once
#include <axiom/set.h>

namespace markup {

class Event;
struct IEventListener;

/**
 *
 */
class Event {
// Fields
private:

// Constructors/destructor
protected:
  Event() {}

public:
  virtual ~Event() {}

private:
  Event(const Event&);

  Event& operator=(const Event&);
};

/**
 *
 */
struct IEventListener {
protected:
  virtual ~IEventListener() {}
};

/**
 * 
 */
template<class T>
class Listeners {
private:
  axiom::set<T*> listeners;

public:
  typedef typename axiom::iterator<T*> iterator;

public:
  void add(T* pListener) {
    if (pListener != NULL) {
      listeners.add(pListener);
    }
  }

  inline void remove(T* pListener) {
    if (pListener != NULL) {
      listeners.remove(pListener);
    }
  }
};

} // end of markup