#pragma once
#include <axiom/cstring.h>
#include <axiom/array.h>

#include "markup/geom.h"
#include "markup/event.h"

namespace markup {

struct IModel;
struct IModelOperation;
struct IModelCommit;

struct IModelObject;
struct IPolygonObject;

struct IModelListener;
struct IModelOperationListener;

class ModelEvent;
class ModelOperationEvent;

/**
 * Тип элемента разметки
 */
enum ModelObjectType {
  MOT_POLYGON,
};

enum ModelObjectStatus {
  MOS_DELETED = 0x0001,
  MOS_EXAMPLE = 0x0002,
};

/**
 * Элемент модели разметки
 */
struct IModelObject {
  virtual const axiom::cstring& name() const = 0;

  virtual ModelObjectType type() const = 0;

  /**
   * комбинация ModelObjectStatus
   */
  virtual int status() const = 0;

  inline bool isExample() const {
    return 0 != (MOS_EXAMPLE & status());
  }

  /**
   * охватывающий прямоугольник
   */
  virtual const Box& getBoundingBox() const = 0;

  virtual void move(double dx, double dy) = 0;

protected:
  virtual ~IModelObject() {}
};

/**
 *
 */
struct IPolygonObject : virtual public IModelObject {
  virtual const Polygon& getPolygon() const = 0;

  virtual void setPolygon(const Polygon& polygon) = 0;

protected:
  virtual ~IPolygonObject() {}
};

/**
 *
 */
struct IModel {
  virtual const axiom::cstring& name() const = 0;

  /**
   * Возвращает объект по имени
   */
  virtual const IModelObject* getObject(const axiom::cstring& name) const = 0;

  /**
   * Возвращает все элементы модели
   */
  virtual axiom::array<const IModelObject*> getObjects() const = 0;

  /**
   * Ищет объекты "под указанной точкой"
   */
  virtual axiom::array<const IModelObject*> locateObjects(const Point& point) const = 0;

  /**
   * Ищет объекты в заданной области
   * @rect - нормализованный прямоугольник области поиска
   */
  virtual axiom::array<const IModelObject*> locateObjects(const Box& box, bool partial) const = 0;

  /**
   * Возвращает шаблон элемента модели по типу
   * @return never NULL
   */
  virtual const IModelObject* getExample(ModelObjectType type) const = 0;

  /**
   * Добавляет слушателя модели
   */
  virtual void addListener(IModelListener* pListener) = 0;

  /**
   * Удаляет слушателя модели
   */
  virtual void removeListener(IModelListener* pListener) = 0;

  /**
   * Создаёт новую операцию над моделью
   */
  virtual IModelOperation* createModelOperation() = 0;

  /**
   * Возвращает список активных операций над моделью
   */
  virtual axiom::array<const IModelOperation*> getOperations() const = 0;

protected:
  virtual ~IModel() {}
};

/**
 * Фильтр по объектам в операции
 */
enum ModelOperationObjectFilter {
  MOOF_CREATED = 0x01,
  MOOF_UPDATED = 0x02,
  MOOF_DELETED = 0x04,
  MOOF_ANY = MOOF_CREATED | MOOF_UPDATED | MOOF_DELETED
};

/**
 * Операция над моделью
 * Позволяет транзакционным образом вносить ряд изменений в модель
 * (с трансляцией изменений слушателям модели)
 */
struct IModelOperation {
  virtual const IModel* getModel() const = 0;

  virtual void addListener(IModelOperationListener* pListener) = 0;

  virtual void removeListener(IModelOperationListener* pListener) = 0;

  /**
   * Возвращает объекты, участвующие в операции
   * @param filter - фильтр по объектам, см. ModelOperationObjectFilter
   */
  virtual axiom::array<const IModelObject*> getObjects(int filter) const = 0;

  virtual IModelObject* createObject(IModelObject* pExample) = 0;

  virtual IModelObject* modifyObject(const IModelObject* pModelObject) = 0;

  virtual void deleteObject(const IModelObject* pModelObject) = 0;

  virtual IModelCommit* commit() = 0;

  virtual void rollback() = 0;

protected:
  virtual ~IModelOperation() {}
};

/**
 * Результат коммита в модель
 */
struct IModelCommit {
  virtual IModel* getModel() const = 0;

  /**
   * Возвращает изменённые в коммите объекты
   * @param filter - фильтр по объектам, см. ModelOperationObjectFilter
   */
  virtual axiom::array<const IModelObject*> getObjects(int filter) const = 0;

protected:
  virtual ~IModelCommit() {}
};

/**
 * Слушатель событий модели
 */
struct IModelListener : public IEventListener {
  virtual void operationStarted(const ModelEvent& modelEvent) = 0;

  virtual void operationCommitted(const ModelEvent& modelEvent) = 0;

  virtual void operationRolledBack(const ModelEvent& modelEvent) = 0;

protected:
  virtual ~IModelListener() {}
};

/**
 * Слушатель операции
 */
struct IModelOperationListener : public IEventListener {
  virtual void objectCreated(const ModelOperationEvent& operationEvent) = 0;

  virtual void objectUpdated(const ModelOperationEvent& operationEvent) = 0;

//    virtual void objectDeleted(ModelEvent& operationEvent)=0;

protected:
  virtual ~IModelOperationListener() {}
};

/**
 * Событие в модели
 */
class ModelEvent : public Event {
// Fields
private:
  mutable IModel* m_pModel;
  mutable IModelOperation* m_pOperation;
  mutable IModelCommit* m_pCommit;

// Constructors/destructor
public:
  explicit ModelEvent(IModel* pModel) :
    m_pModel(pModel),
    m_pOperation(NULL),
    m_pCommit(NULL) {}

  explicit ModelEvent(IModelOperation* pOperation) :
    m_pModel(pOperation->getModel()),
    m_pOperation(pOperation),
    m_pCommit(NULL) {}

  ModelEvent(IModelCommit* pCommit, IModelOperation* pOperation) :
    m_pModel(pCommit->getModel()),
    m_pOperation(pOperation),
    m_pCommit(pCommit) {}

// Methods
public:
  IModel* getModel() const { return m_pModel; }

  IModelCommit* getCommit() const  { return m_pCommit; }

  IModelOperation* getOperation() const { return m_pOperation; }
};

/**
 * Событие в операции над моделью
 */
class ModelOperationEvent : public Event {
// Fields
private:

// Constructors/destructor
public:

// Methods
public:
};

}; // end of markup