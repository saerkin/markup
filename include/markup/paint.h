#pragma once
#include "markup/geom.h"
#include "markup/graphics.h"

namespace markup {

// Перечень интерфейсов и классов
struct IGraphicsContext;
struct IPainter;

/**
 * Графический контекст
 */
struct IGraphicsContext {
  virtual void drawLine(
    const Point& from,
    const Point& to,
    const Pen& pen) = 0;

  virtual void drawRectangle(
    const Box& rect,
    const Pen& boundingPen,
    const Brush& fillingBrush) = 0;

  /**
   * Рисует букву в заданном прямоугольнике
   * с выравниванием по центру
   */
  virtual void drawChar(
    char c,
    const Box& rect,
    const Font& font,
    const Color& color) = 0;

protected:
  virtual ~IGraphicsContext() {}
};

/**
 * 
 */
struct IPainter {
  virtual void paint(IGraphicsContext& gc) = 0;

protected:
  virtual ~IPainter() {}
};

} // end of markup
