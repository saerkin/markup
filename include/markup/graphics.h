#pragma once

#include "markup/geom.h"
#include <axiom/cstring.h>

namespace markup {

class Color;
class Pen;
class Brush;
class Font;

/**
 * 
 */
class Color {
// Fields
public:
  const static Color TRANSPARENT;

  const static Color BLACK;

  const static Color WHITE;

  const static Color GREEN;

private:
  int m_RValue;
  int m_GValue;
  int m_BValue;

// Constructors/destructor
public:
  inline Color()
    : m_RValue(0), m_GValue(0), m_BValue(0) {}

  inline Color(int r, int g, int b)
    : m_RValue(r), m_GValue(g), m_BValue(b) {}

// Methods
public:
  inline int red() const { return m_RValue; }

  inline int green() const { return m_GValue; }

  inline int blue() const { return m_BValue; }

// Operators
public:
  inline bool operator==(const Color& color) const {
    return
      this->m_RValue == color.m_RValue
        && this->m_GValue == color.m_GValue
        && this->m_BValue == color.m_BValue;
  }
};

enum PenStyle {
  PENSTYLE_SOLID,
  PENSTYLE_DASH,
  PENSTYLE_TRIO_DASH
};

/**
 * 
 */
class Pen {
// Fields
private:
  Color m_color;
  double m_width;
  PenStyle m_style;

// Constructors/destructor
public:
  inline Pen(PenStyle style, double width, const Color& color)
    : m_style(style), m_width(width), m_color(color) {}

  inline Pen(double width, const Color& color)
    : m_style(PENSTYLE_SOLID), m_width(width), m_color(color) {}

// Methods
public:
  inline PenStyle style() const { return m_style; }

  inline double width() const { return m_width; }

  inline const Color& color() const { return m_color; }
};

/**
 * 
 */
class Brush {
// Fields
private:
  Color m_color;

// Constructors/destructor
public:
  inline explicit Brush(const Color& color)
    : m_color(color) {}

// Methods
public:
  inline const Color& color() const { return m_color; }
};

/**
 * 
 */
class Font {
// Fields
public:
  const static Font DEFAULT;

private:
  axiom::cstring m_faceName;
  int m_height;

// Constructors/destructor
public:
  inline explicit Font(const axiom::cstring& faceName, int height)
    : m_faceName(faceName), m_height(height) {}

  inline Font(const Font& font)
    : m_faceName(font.faceName()), m_height(font.height()) {}

// Methods
public:
  inline const axiom::cstring& faceName() const { return m_faceName; }

  inline int height() const { return m_height; }

  inline axiom::cstring toString() const {
    return m_faceName + "," + m_height + "px";
  }

// Operators
private:
  Font& operator=(const Font& font);

public:
  inline bool operator==(const Font& font) const {
    return
      0 == m_faceName.compareIgnoreCase(font.m_faceName)
        && m_height == font.m_height;
  }
};

} // end of markup