#pragma clang diagnostic push
#pragma ide diagnostic ignored "misc-non-private-member-variables-in-classes"
#pragma once

#include "markup/geom.h"
#include "markup/event.h"
#include "markup/paint.h"

namespace markup {

// Перечень интерфейсов и классов
struct IWindow;
class AbstractWindow;

struct IMouseListener;
struct IKeyListener;

class InputEvent;
class MouseEvent;
class KeyEvent;

enum MouseCursor {
  MOUSECURSOR_DEFAULT = 0,
  MOUSECURSOR_MOVE,
  MOUSECURSOR_RESIZE_NESW,
  MOUSECURSOR_RESIZE_NS,
  MOUSECURSOR_RESIZE_NWSE,
  MOUSECURSOR_RESIZE_WE,
};

/**
 * Окно
 */
struct IWindow {
  /**
   * Художник рисует содержимое окна.
   * Для одного окна может быть только один художник.
   */
  virtual IPainter* getPainter() const = 0;

  virtual void setPainter(IPainter* pPainter) = 0;

  // Keyboard&Mouse workaround ----------------

  virtual IMouseListener* getMouseListener() const = 0;

  virtual void setMouseListener(IMouseListener* pMouseListener) = 0;

  virtual IKeyListener* getKeyListener() const = 0;

  virtual void setKeyListener(IKeyListener* pKeyListener) = 0;

  /**
   * point - точка старта drag&drop в координатах разметки
   */
  virtual bool detectDrag(const Point& point) = 0;

  virtual void captureMouse() = 0;

  virtual void releaseMouse() = 0;

  virtual void setMouseCursor(MouseCursor cursor) = 0;

  virtual bool isKeyPressed(int key) = 0;

  // Focus box workaround ---------------------

  virtual const Box& getFocusBox() const = 0;

  virtual void setFocusBox(const Box& focusBox) = 0;

  virtual void showFocusBox(bool show) = 0;

  // Window content updating ------------------

  virtual void invalidate() = 0;

protected:
  virtual ~IWindow() {}
};

/**
 * Слушатель событий от мыши
 */
struct IMouseListener : public IEventListener {
  virtual void mousePressed(const MouseEvent& mouseEvent) = 0;

  virtual void mouseMoved(const MouseEvent& mouseEvent) = 0;

  virtual void mouseReleased(const MouseEvent& mouseEvent) = 0;

  virtual void mouseClicked(const MouseEvent& mouseEvent) = 0;

protected:
  virtual ~IMouseListener() {}
};

/**
 * Слушатель событий от клавиатуры
 */
struct IKeyListener : public IEventListener {
  virtual void keyPressed(const KeyEvent& keyEvent) = 0;

  virtual void keyReleased(const KeyEvent& keyEvent) = 0;

protected:
  virtual ~IKeyListener() {}
};

/**
 * 
 */
class InputEvent : public Event {
public:
  enum {
    NOBUTTON = 0,
    LBUTTON,
    RBUTTON,
    MBUTTON,

    ALT,
    CTRL,
    SHIFT,
    LEFT,
    RIGHT,
    UP,
    DOWN
  };

// Fields
private:

// Constructors/destructor
protected:
  inline InputEvent() {}

// Methods
public:
};

/**
 * 
 */
class MouseEvent : public InputEvent {
// Fields
private:
  const Point m_point;
  const int m_button;
  const int m_clickCount;

// Constructors/destructor
public:
  inline explicit MouseEvent(const Point& point)
    : m_point(point), m_button(NOBUTTON), m_clickCount(0) {}

  inline MouseEvent(const Point& point, int button)
    : m_point(point), m_button(button), m_clickCount(0) {}

  inline MouseEvent(const Point& point, int button, int clickCount)
    : m_point(point), m_button(button), m_clickCount(clickCount) {}

// Methods
public:
  inline const Point& point() const {
    return m_point;
  }

  inline int button() const {
    return m_button;
  }

  /**
   * Количество кликов в случае mouseClicked()
   */
  inline int clickCount() const {
    return m_clickCount;
  }
};

/**
 * 
 */
class KeyEvent : public InputEvent {
// Fields
private:
  int m_keyCode;

// Constructors/destructor
public:
  inline explicit KeyEvent(int keyCode)
    : m_keyCode(keyCode) {}

// Methods
public:
  inline int keyCode() const {
    return m_keyCode;
  }
};

} // end of markup

#pragma clang diagnostic pop