#pragma once

#include <cassert>

#include "markup/window.h"

namespace markup {

/**
 *
 */
class AbstractWindow : public IWindow {
// Fields
private:
  mutable IPainter* m_pPainter;
  mutable IMouseListener* m_pMouseListener;
  mutable IKeyListener* m_pKeyListener;

// Constructors/destructor
protected:
  inline AbstractWindow() :
    m_pPainter(NULL),
    m_pMouseListener(NULL),
    m_pKeyListener(NULL) {}

  ~AbstractWindow() {}

private:
  AbstractWindow(const AbstractWindow&);

// Operators
private:
  AbstractWindow& operator=(const AbstractWindow&);

// Methods
private:
  IMouseListener* getMouseListener() const {
    return m_pMouseListener;
  }

  void setMouseListener(IMouseListener* pMouseListener) {
    assert(pMouseListener);
    m_pMouseListener = pMouseListener;
  }

  IKeyListener* getKeyListener() const {
    return m_pKeyListener;
  }

  void setKeyListener(IKeyListener* pKeyListener) {
    m_pKeyListener = pKeyListener;
  }

  IPainter* getPainter() const {
    return m_pPainter;
  }

  void setPainter(IPainter* pPainter) {
    m_pPainter = pPainter;
  }
};

}; // end of markup