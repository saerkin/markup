#pragma once

#include <cassert>
#include <windows.h>
#include <axiom/lifescope.h>

#include "markup/geom.h"
#include "markup/window.h"
#include "markup/impl/window/AbstractWindow.h"

namespace markup {

/**
 * 
 */
class WinAPIWindow : public AbstractWindow {
// Fields
private:
  HWND m_hwnd;
  HCURSOR m_hDefaultCursor;
  Box m_focusBox;
  bool m_focusBoxShowed;
  Transform m_markup2screen;
  Transform m_screen2markup;

// Constructors/destructor
public:
  WinAPIWindow();

protected:
  virtual ~WinAPIWindow() {}

private:
  WinAPIWindow(const WinAPIWindow&);

// Operators
private:
  WinAPIWindow& operator=(const WinAPIWindow&);

// Attributes
public:
  inline void setHandle(HWND hwnd) {
    assert(hwnd);
    m_hwnd = hwnd;
  }

// IWindow methods
private:
  bool detectDrag(const Point& point);

  void captureMouse();

  void releaseMouse();

  void setMouseCursor(MouseCursor cursor);

  bool isKeyPressed(int key);

  inline const Box& getFocusBox() const {
    return m_focusBox;
  }

  inline void setFocusBox(const Box& focusBox) {
    m_focusBox = focusBox;
  }

  void showFocusBox(bool show);

  void invalidate();

//
public:
  LRESULT handleWindowMessage(UINT message, WPARAM wParam, LPARAM lParam);

private:
  void prepareDC(HDC hdc);
};

} // end of markup