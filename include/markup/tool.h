#pragma once
#include <axiom/cstring.h>
#include <axiom/array.h>

#include "markup/geom.h"
#include "markup/event.h"
#include "markup/model.h"

namespace markup {

// Перечень интерфейсов и классов
struct ITool;
struct ISelectTool;
struct IMoveTool;
struct ISizeTool;
struct IPolygonTool;

struct IControlKnob;

struct IToolListener;

class ToolEvent;

/*extern*/ struct IEditor;

/**
 *
 */
struct ITool {
  virtual const axiom::cstring name() const = 0;

  virtual IEditor* getEditor() const = 0;

  virtual void addListener(IToolListener* pListener) = 0;

  virtual void removeListener(IToolListener* pListener) = 0;

  /**
   * todo: wtf?
   */
  virtual const IControlKnob* getControlKnob(const Point& point) const = 0;

protected:
  virtual ~ITool() {}
};

/**
 * Инструмент выделения объектов
 */
struct ISelectTool : virtual public ITool {
protected:
  virtual ~ISelectTool() {}
};

/**
 * Инструмент перемещения объектов
 */
struct IMoveTool : virtual public ITool {
protected:
  virtual ~IMoveTool() {}
};

/**
 * Инструмент растягивания объектов
 */
struct ISizeTool : virtual public ITool {
protected:
  virtual ~ISizeTool() {}
};

/**
 * Инструмент по многоугольнику:
 * - создание многоугольника
 * - редактирование вершин многоугольника
 */
struct IPolygonTool : virtual public ITool {
protected:
  virtual ~IPolygonTool() {}
};

/**
 * Рукоятка управления инструмента
 * todo: wtf?
 */
struct IControlKnob {
protected:
  virtual ~IControlKnob() {}
};

/**
 * Слушатель инструмента
 */
struct IToolListener : public IEventListener {
  virtual void actionPerformed(const ToolEvent& event) = 0;

protected:
  virtual ~IToolListener() {}
};

/**
 * Событие в работе инструмента
 */
class ToolEvent : public Event {
// Fields
private:
  mutable ITool* m_pTool;

// Constructors/destructor
public:
  inline explicit ToolEvent(ITool* pTool) : m_pTool(pTool) {}

private:
  ToolEvent(const ToolEvent&);
  ToolEvent& operator=(const ToolEvent&);

// Methods
public:
  inline ITool* getTool() const { return m_pTool; }

  inline IEditor* getEditor() const { return m_pTool->getEditor(); }
};

} // end of markup