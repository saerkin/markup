#pragma once

#include <axiom/lifescope.h>
#include <axiom/iterator.h>
#include <axiom/cstring.h>
#include <axiom/array.h>
#include <axiom/set.h>
#include <cassert>

#include "markup/model.h"
#include "markup/tool.h"
#include "markup/window.h"

namespace markup {

// Перечень интерфейсов и классов
// Редактор
struct IEditor;
struct IEditorFactory;
class EditorBuilder;

// События и слушатели
struct IEditorListener;
class NoOpEditorListener;
struct ISelectionListener;
struct ICommand;
struct ICommandListener;
class EditorEvent;
class SelectionEvent;
class CommandEvent;
class CommandState;

// Прочее
class Selection;
enum ModelObjectEditState;

/**
 *
 */
struct IEditor {
  /**
   * Модель, которую редактируем
   */
  virtual IModel* getModel() const = 0;

  virtual IWindow* getWindow() const = 0;

  virtual void setWindow(IWindow* pWindow) = 0;

  /**
   * Запустить редактор
   */
  virtual void start() = 0;

  /**
   * Приостановить редактор
   */
  virtual void stop() = 0;

  /**
   * Перечень команд, доступных пользователю из меню редактора
   */
  virtual axiom::array<ICommand*> getCommands() const = 0;

  virtual ICommand* getCommand(const axiom::cstring& codename) const = 0;

  /**
   * Перечень инструментов, доступных редактору
   */
  virtual axiom::array<const ITool*> getTools() const = 0;

  virtual ITool* getTool(const axiom::cstring& name) const = 0;

  virtual ITool* getActiveTool() const = 0;

  virtual void setActiveTool(ITool* pTool) = 0;

  virtual void setActiveTool(const axiom::cstring& name) = 0;

  virtual ISelectTool* getSelectTool() = 0;

  virtual IMoveTool* getMoveTool() = 0;

  virtual ISizeTool* getSizeTool() = 0;

  /**
   * Текущая селекция
   */
  virtual Selection getSelection() const = 0;

  virtual void setSelection(const Selection& selection) = 0;

  virtual void addSelectionListener(ISelectionListener* pListener) = 0;

  virtual void removeSelectionListener(ISelectionListener* pListener) = 0;

  /**
   * Ищет выделенные объекты "под указанной точкой"
   */
  virtual axiom::array<const IModelObject*> locateSelectedObjects(const Point& point) const = 0;

protected:
  virtual ~IEditor() {}
};

/**
 *
 */
struct IEditorFactory {
  /**
   * Создаёт новый редактор.
   * @param getLifeScope - контекст, в котором должен быть сохранён новый редактор
   * @param pModel - модель, для которой требуется создать редактор
   */
  virtual IEditor* createEditor(axiom::LifeScope& lifeScope, IModel* pModel) = 0;

protected:
  virtual ~IEditorFactory() {}
};

/**
 *
 */
struct IEditorListener : public IEventListener {
  virtual void editorStarted(const EditorEvent& editorEvent) = 0;

  virtual void editorStopped(const EditorEvent& editorEvent) = 0;

  virtual void activeToolChanged(const EditorEvent& editorEvent) = 0;

protected:
  virtual ~IEditorListener() {}
};

/**
 * 
 */
class NoOpEditorListener : public IEditorListener {
protected:
  virtual void editorStarted(const EditorEvent& editorEvent) {}

  virtual void editorStopped(const EditorEvent& editorEvent) {}

  virtual void activeToolChanged(const EditorEvent& editorEvent) {}
};

/**
 * 
 */
struct ISelectionListener : public IEventListener {
  virtual void selectionChanged(const SelectionEvent& selectionEvent) = 0;

protected:
  virtual ~ISelectionListener() {}
};

/**
 * 
 */
struct ICommand {
  virtual const axiom::cstring& name() const = 0;

  virtual CommandState state() const = 0;

  virtual void perform() = 0;

protected:
  virtual ~ICommand() {}
};

/**
 * 
 */
struct ICommandListener : public IEventListener {
  virtual void updateCommandState(CommandState& commandState) = 0;

  virtual void commandPerformed(CommandEvent& commandEvent) = 0;

protected:
  virtual ~ICommandListener() {}
};

/**
 * 
 */
class EditorEvent : public Event {
// Fields
private:
  mutable IEditor* m_pEditor;

// Constructors/destructor
public:
  inline explicit EditorEvent(IEditor* pEditor)
    : m_pEditor(pEditor) {}

// Methods
public:
  inline IEditor* getEditor() const {
    return m_pEditor;
  }
};

/**
 * 
 */
class SelectionEvent : public Event {
};

/**
 *
 */
class CommandEvent : public Event {
// Fields
private:
  mutable ICommand* m_pCommand;

// Constructors/destructor
public:
  inline explicit CommandEvent(ICommand* pCommand)
    : m_pCommand(pCommand) {}

private:
  CommandEvent(const CommandEvent&);
  CommandEvent& operator=(const CommandEvent&);

// Methods
public:
  inline ICommand* getCommand() const {
    return m_pCommand;
  }
};

/**
 * 
 */
class CommandState {
// Fields
private:
  const ICommand* m_pCommand;
  axiom::cstring m_text;
  bool m_enabled;

// Constructors/destructor
public:
  inline explicit CommandState(const ICommand* pCommand)
    : m_pCommand(pCommand), m_enabled(false) {}

// Methods
public:
  inline const ICommand* getCommand() const {
    return m_pCommand;
  }

  inline bool isEnabled() const {
    return m_enabled;
  }

  inline void setEnabled(bool enabled) {
    m_enabled = enabled;
  }

  inline const axiom::cstring& getText() const {
    return m_text;
  }

  inline void setText(const axiom::cstring& text) {
    m_text = text;
  }
};

/**
 * Группа селектированных объектов
 * todo: не следует ли понятие селекции добавить в модель?
 * Mutable
 */
class Selection {
// Types
public:
  typedef axiom::iterator<const IModelObject*> iterator;

// Fields
private:
  axiom::set<const IModelObject*> m_objects;

// Constructors/destructor
public:
  inline Selection() {}

  inline explicit Selection(const IModelObject* pObject) {
    assert (pObject);
    m_objects = m_objects.add(pObject);
  }

  inline explicit Selection(const axiom::array<const IModelObject*>& objects)
    : m_objects(objects) {}

// Operators
public:
  inline bool operator==(const Selection& selection) const {
    return this == &selection || m_objects == selection.m_objects;
  }

  Selection& operator=(const Selection& other) {
    if (this != &other)
      m_objects = other.m_objects;
    return *this;
  }

// Methods
public:
  inline int size() const {
    return m_objects.size();
  }

  inline bool isEmpty() const {
    return m_objects.isEmpty();
  }

  inline iterator begin() const {
    return m_objects.begin();
  }

  inline iterator begin() {
    return m_objects.begin();
  }

  inline bool contains(const IModelObject* pObject) const {
    return m_objects.has(pObject);
  }

  inline bool remove(const IModelObject* pObject) {
    axiom::set<const IModelObject*> objects = m_objects.remove(pObject);
    bool changed = (objects != m_objects);
    m_objects = objects;
    return changed;
  }

//  inline void clear() {
//    m_objects = axiom::set<const IModelObject*>();
//  }
};

/**
 *
 */
class EditorBuilder {
// Fields
private:
  /**
   * Контекст, куда складываются объекты, созданные билдером
   * Переходит в редактор при его создании
   */
  axiom::LifeScope m_lifeScope;

  mutable IModel* m_pModel;

  axiom::array<IEditorListener*> m_editorListeners;

  axiom::array<ITool*> m_tools;

  axiom::array<ICommand*> m_commands;

// Constructors/destructor
public:
  EditorBuilder();

private:
  EditorBuilder(const EditorBuilder&);
  EditorBuilder& operator=(const EditorBuilder&);

// Attributes
public:
  axiom::LifeScope& lifeScope() { return m_lifeScope; }

// Methods
public:
  void setModel(IModel* pModel);

  ISelectTool* addSelectTool(const axiom::cstring& name = "");

  IMoveTool* addMoveTool(const axiom::cstring& name = "");

  ISizeTool* addSizeTool(const axiom::cstring& name = "");

  IPolygonTool* addFormFieldTool(const axiom::cstring& name = "");

  ITool* addQuadObjectTool(const axiom::cstring& name = "");

  void addEditorListener(IEditorListener* pEditorListener);

  ICommand* addCommand(const axiom::cstring& codename, ICommandListener* pListener);

  IEditor* build(axiom::LifeScope& lifeScope);
};

/**
 * Статусы объекта в редакторе
 */
enum ModelObjectEditState {
  MOES_SELECTED = 0x01,
};

} // end of markup