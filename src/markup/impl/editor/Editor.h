#pragma once

#include "markup/model.h"
#include "markup/view.h"
#include "markup/editor.h"
#include "markup/window.h"
#include "markup/impl/editor/tool/Tool.h"
//#include "markup/impl/controller/Command.h"

#include <axiom/lifescope.h>
#include <axiom/cstring.h>
#include <axiom/array.h>
#include <cassert>

namespace markup {

/**
 *
 */
class Editor :
  public IEditor,
  public IModelListener,
  public IModelOperationListener,
  public IMouseListener,
  public IKeyListener,
  public axiom::LifeScopeObject {
// Fields
private:
  /**
   * Собственный life scope редактора
   */
  axiom::LifeScope m_lifeScope;

  mutable IView* m_pView;
  mutable IModel* m_pModel;
  mutable IWindow* m_pWindow;

  axiom::array<IEditorTool*> m_tools;
  IEditorTool* m_pSelectTool;
  IEditorTool* m_pMoveTool;
  IEditorTool* m_pSizeTool;
  IEditorTool* m_pActiveTool;

  axiom::array<ICommand*> m_commands;

  Selection m_selection;

  bool m_started;
  bool m_dragStarted;
  bool m_clickin;

  axiom::set<IEditorListener*> m_editorListeners;
  axiom::set<ISelectionListener*> m_selectionListeners;

// Constructors/destructor
public:
  explicit Editor(axiom::LifeScope& lifeScope);

private:
  Editor(const Editor& e);

private:
  ~Editor() {}

// Operators
private:
  Editor& operator=(const Editor& e);

// Attributes
public:
  inline axiom::LifeScope& lifeScope() {
    return m_lifeScope;
  }

  inline IModel* getModel() const {
    return m_pModel;
  }

  inline void setModel(IModel* pModel) {
    assert (!m_started);
    m_pModel = pModel;
  }

  inline IView* getView() const {
    return m_pView;
  }

  inline void setView(IView* pView) {
    assert (!m_started);
    m_pView = pView;
  }

  inline IWindow* getWindow() const {
    return m_pWindow;
  }

  inline void setWindow(IWindow* pWindow) {
    assert (!m_started);
    m_pWindow = pWindow;
  }

  axiom::array<const ITool*> getTools() const;

  void setTools(const axiom::array<ITool*>& tools);

  ITool* getTool(const axiom::cstring& name) const;

  ITool* getActiveTool() const;

  void setActiveTool(ITool* pTool);

  void setActiveTool(const axiom::cstring& name);

  ISelectTool* getSelectTool();

  IMoveTool* getMoveTool();

  ISizeTool* getSizeTool();

  axiom::array<ICommand*> getCommands() const {
    return m_commands;
  }

  void setCommands(const axiom::array<ICommand*>& commands);

  ICommand* getCommand(const axiom::cstring& name) const;

  inline Selection getSelection() const {
    return m_selection;
  }

  void setSelection(const Selection& selection);

  inline void addSelectionListener(ISelectionListener* pListener) {
    m_selectionListeners = m_selectionListeners.add(pListener);
  }

  inline void removeSelectionListener(ISelectionListener* pListener) {
    m_selectionListeners = m_selectionListeners.remove(pListener);
  }

  axiom::array<const IModelObject*> locateSelectedObjects(const Point& point) const;

  /**
   * todo: move to ctor
   */
  void setListeners(const axiom::array<IEditorListener*>& listeners);

// Operations
public:
  void start();
  void stop();

private:
  void fireEditorStartedEvent();
  void fireEditorStoppedEvent();
  void fireActiveToolChangedEvent();
  void fireSelectionChangedEvent();

  void invalidate();

  bool startDrag(const Point& point);

  /**
   * Возвращает инструмент, наиболее подходящий в данной точке
   */
  IEditorTool* getDragTool(const Point& point);

  void updateMouseCursor(const Point& point);

  /**
   * Remove dead objects from selection, refresh updated objects in selection
   */
  void updateSelection();

// IModelListener methods
private:
  void operationStarted(const ModelEvent& modelEvent);
  void operationCommitted(const ModelEvent& modelEvent);
  void operationRolledBack(const ModelEvent& modelEvent);

// IModelOperationListener methods
private:
  void objectCreated(const ModelOperationEvent& operationEvent);
  void objectUpdated(const ModelOperationEvent& operationEvent);

// IMouseListener methods
private:
  void mousePressed(const MouseEvent& mouseEvent);
  void mouseMoved(const MouseEvent& mouseEvent);
  void mouseReleased(const MouseEvent& mouseEvent);
  void mouseClicked(const MouseEvent& mouseEvent) {}

// IKeyListener methods
private:
  void keyPressed(const KeyEvent& keyEvent);
  void keyReleased(const KeyEvent& keyEvent);
};

} // end of markup