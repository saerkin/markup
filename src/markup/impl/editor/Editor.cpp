#include "precomp.h"
#include "markup/impl/editor/Editor.h"
#include "markup/impl/editor/Command.h"

namespace markup {

using namespace axiom;

//typedef EventDispatcher::TEventCallback<EditorEvent, IEditorListener> EditorEventCallback;
//
//typedef EventDispatcher::TEventCallback<SelectionEvent, ISelectionListener> SelectionEventCallback;

Editor::Editor(LifeScope& lifeScope) : LifeScopeObject(lifeScope) {
  m_pModel = NULL;
  m_pView = NULL;
  m_pWindow = NULL;
  m_pActiveTool = NULL;
  m_started = false;
  m_dragStarted = false;
  m_clickin = false;

  m_pSelectTool = NULL;
  m_pMoveTool = NULL;
  m_pSizeTool = NULL;
}

//void Editor::getTools(list<ITool*>& tools) const {
//  ListUtils::addAll(tools, m_tools);
//}

void Editor::setTools(const array<ITool*>& tools) {
  assert (!m_started);

  m_tools = array<IEditorTool*>();
  iterator<ITool*> it = tools.begin();
  while (it.hasNext()) {
    IEditorTool* pTool = dynamic_cast<IEditorTool*>(it.next());
    pTool->setEditor(this);
    m_tools = m_tools.add(pTool);

    if (NULL != dynamic_cast<ISelectTool*>(pTool))
      m_pSelectTool = pTool;
    if (NULL != dynamic_cast<IMoveTool*>(pTool))
      m_pMoveTool = pTool;
    if (NULL != dynamic_cast<ISizeTool*>(pTool))
      m_pSizeTool = pTool;
  }
}

void Editor::setListeners(const array<IEditorListener*>& listeners) {
  assert (!m_started);
  m_editorListeners = set<IEditorListener*>(listeners);
}

//////////////////////////////////////////////////////////////////////////
// Editor attributes

ITool* Editor::getTool(const cstring& name) const {
  iterator<IEditorTool*> it = m_tools.begin();
  while (it.hasNext()) {
    IEditorTool* pTool = it.next();
    if (name == pTool->name())
      return pTool;
  }
  return NULL;
}

ITool* Editor::getActiveTool() const {
  return m_pActiveTool;
}

void Editor::setActiveTool(ITool* pTool) {
  assert (pTool);
  if (pTool == m_pActiveTool)
    return;

  IEditorTool* pEditorTool = dynamic_cast<IEditorTool*>(pTool);
  assert (pEditorTool && m_tools.has(pEditorTool));

  IEditorTool* pOldActiveTool = m_pActiveTool;
  m_pActiveTool = pEditorTool;

  if (pOldActiveTool != NULL)
    pOldActiveTool->stop();
  if (m_pActiveTool != NULL)
    m_pActiveTool->start();

  fireActiveToolChangedEvent();
}

void Editor::setActiveTool(const cstring& name) {
  assert (!name.isEmpty());
  ITool* pTool = getTool(name);
  assert (pTool);
  setActiveTool(pTool);
}

ISelectTool* Editor::getSelectTool() {
  return dynamic_cast<ISelectTool*>(m_pSelectTool);
}

IMoveTool* Editor::getMoveTool() {
  return dynamic_cast<IMoveTool*>(m_pMoveTool);
}

ISizeTool* Editor::getSizeTool() {
  return dynamic_cast<ISizeTool*>(m_pSizeTool);
}

void Editor::setCommands(const array<ICommand*>& commands) {
  assert (!m_started);

  m_commands = m_commands.clear();
  iterator<ICommand*> it = commands.begin();
  while (it.hasNext()) {
    Command* pCommand = dynamic_cast<Command*>(it.next());
    pCommand->setEditor(this);
    m_commands = m_commands.add(pCommand);
  }
}

ICommand* Editor::getCommand(const cstring& name) const {
  iterator<ICommand*> it = m_commands.begin();
  while (it.hasNext()) {
    ICommand* pCommand = it.next();
    if (pCommand->name() == name)
      return pCommand;
  }
  return NULL;
}

void Editor::setSelection(const Selection& selection) {
  if (m_selection == selection)
    return;

  m_selection = selection;
  invalidate();
  fireSelectionChangedEvent();
}

array<const IModelObject*> Editor::locateSelectedObjects(const Point& point) const {
  array<const IModelObject*> selection;
  Selection::iterator it = m_selection.begin();
  while (it.hasNext()) {
    const IModelObject* pObject = it.next();
    Box box = pObject->getBoundingBox();
    box = box.extend(3, 3);
    if (box.contains(point))
      selection = selection.add(pObject);
  }
  return selection;
}

//////////////////////////////////////////////////////////////////////////
// Editor operations

void Editor::start() {
  assert (m_pModel);
  assert (m_pView);
  if (m_started)
    return;

  m_dragStarted = false;
  m_clickin = false;

  m_pModel->addListener(this);
  if (m_pWindow != NULL) {
    m_pWindow->setPainter(m_pView);
    m_pWindow->setMouseListener(this);
    m_pWindow->setKeyListener(this);
  }

  if (m_pActiveTool != NULL)
    m_pActiveTool->start();

  fireEditorStartedEvent();

  m_started = true;

  updateSelection();
}

void Editor::stop() {
  if (!m_started)
    return;

  fireEditorStoppedEvent();

  if (m_pActiveTool != NULL)
    m_pActiveTool->stop();

  if (m_pWindow != NULL) {
    m_pWindow->setKeyListener(this);
    m_pWindow->setMouseListener(this);
    m_pWindow->setPainter(NULL);
  }
  m_pModel->removeListener(this);

  m_started = false;
  m_dragStarted = false;
  m_clickin = false;
}

void Editor::invalidate() {
  if (m_pWindow != NULL)
    m_pWindow->invalidate();
}

bool Editor::startDrag(const Point& point) {
  assert (!m_dragStarted);

  if (m_pWindow != NULL && m_pWindow->detectDrag(point)) {
    do {
      // 1. Подберём инструмент для drag&drop в указанной точке
      IEditorTool* pDragTool = getDragTool(point);
      if (pDragTool == NULL)
        break;

      // 2. Сделаем выбранный инструмент активным
      setActiveTool(pDragTool);

      // 3. Начнём drag&drop
      if (!pDragTool->startDrag(point))
        break;

      m_pWindow->captureMouse();
      m_dragStarted = true;
    } while (false);
  }

  return m_dragStarted;
}

IEditorTool* Editor::getDragTool(const Point& point) {
  bool baseTool = false;
  if (m_pActiveTool != NULL) {
    baseTool = baseTool || m_pActiveTool == m_pSelectTool;
    baseTool = baseTool || m_pActiveTool == m_pMoveTool;
    baseTool = baseTool || m_pActiveTool == m_pSizeTool;
  }
  if (!baseTool)
    return m_pActiveTool;

  array<IEditorTool*> tools(m_pSizeTool, m_pMoveTool);
  for (int i = 0; i < tools.size(); i++) {
    IEditorTool* pTool = tools[i];
    if (pTool != NULL && pTool->getControlKnob(point) != NULL)
      return pTool;
  }
  return m_pSelectTool;
}

void Editor::updateMouseCursor(const Point& point) {
  if (m_pWindow == NULL)
    return;

  IEditorTool* pActionTool =
    m_dragStarted
      ? m_pActiveTool
      : getDragTool(point);

  MouseCursor cursor = MOUSECURSOR_DEFAULT;
  if (pActionTool != NULL)
    cursor = pActionTool->getMouseCursor(point);
  m_pWindow->setMouseCursor(cursor);
}

//////////////////////////////////////////////////////////////////////////
// Editor event fires

void Editor::fireEditorStartedEvent() {
  iterator<IEditorListener*> it = m_editorListeners.begin();
  while (it.hasNext()) {
    IEditorListener* pListener = it.next();
    pListener->editorStarted(EditorEvent(this));
  }
}

void Editor::fireEditorStoppedEvent() {
  iterator<IEditorListener*> it = m_editorListeners.begin();
  while (it.hasNext()) {
    IEditorListener* pListener = it.next();
    pListener->editorStopped(EditorEvent(this));
  }
}

void Editor::fireActiveToolChangedEvent() {
  if (!m_started)
    return;

  iterator<IEditorListener*> it = m_editorListeners.begin();
  while (it.hasNext()) {
    IEditorListener* pListener = it.next();
    pListener->activeToolChanged(EditorEvent(this));
  }
}

void Editor::fireSelectionChangedEvent() {
  if (!m_started)
    return;

  iterator<ISelectionListener*> it = m_selectionListeners.begin();
  while (it.hasNext()) {
    ISelectionListener* pListener = it.next();
    pListener->selectionChanged(SelectionEvent());
  }
}

//////////////////////////////////////////////////////////////////////////
// IModelListener methods

void Editor::operationStarted(const ModelEvent& modelEvent) {
  IModelOperation* pModelOperation = modelEvent.getOperation();
  pModelOperation->addListener(this);
}

void Editor::operationCommitted(const ModelEvent& modelEvent) {
  IModelOperation* pModelOperation = modelEvent.getOperation();
  pModelOperation->removeListener(this);
  invalidate();

  updateSelection();
}

void Editor::updateSelection() {
  array<const IModelObject*> objects;
  Selection::iterator it = m_selection.begin();
  while (it.hasNext()) {
    const IModelObject* pOldObject = it.next();
    const IModelObject* pNewObject =
      pOldObject->isExample()
        ? m_pModel->getExample(pOldObject->type())
        : m_pModel->getObject(pOldObject->name());
    if (pNewObject != NULL)
      objects = objects.add(pNewObject);
  }
  setSelection(Selection(objects));
}

void Editor::operationRolledBack(const ModelEvent& modelEvent) {
  IModelOperation* pModelOperation = modelEvent.getOperation();
  pModelOperation->removeListener(this);
  invalidate();
}

//////////////////////////////////////////////////////////////////////////
// IModelOperationListener events

void Editor::objectCreated(const ModelOperationEvent& event) {
  invalidate();
}

void Editor::objectUpdated(const ModelOperationEvent& event) {
  invalidate();
}

//////////////////////////////////////////////////////////////////////////
// IMouseListener events

void Editor::mousePressed(const MouseEvent& event) {
  if (!m_started)
    return;

  m_clickin = true;

  const Point& point = event.point();
  switch (event.button()) {
    case InputEvent::LBUTTON:
      // todo: what if dragDetect() was cancelled with keyboard?
      if (!startDrag(point)) {
        IEditorTool* pClickTool = getDragTool(point);
        if (pClickTool != NULL) {
          setActiveTool(pClickTool);
          pClickTool->click(point);
        }
      }
      break;
  }

  updateMouseCursor(point);
}

void Editor::mouseMoved(const MouseEvent& event) {
  if (!m_started)
    return;

  m_clickin = false;

  const Point& point = event.point();

  if (m_dragStarted) {
    if (m_pActiveTool != NULL)
      m_pActiveTool->continueDrag(point);
  }

  updateMouseCursor(point);
}

void Editor::mouseReleased(const MouseEvent& event) {
  if (!m_started)
    return;

  const Point& point = event.point();

  if (m_dragStarted) {
    m_dragStarted = false;
    if (m_pWindow != NULL)
      m_pWindow->releaseMouse();

    switch (event.button()) {
      case InputEvent::LBUTTON:
        if (m_pActiveTool != NULL)
          m_pActiveTool->completeDrag(point);
        break;
      case InputEvent::RBUTTON:
        if (m_pActiveTool != NULL)
          m_pActiveTool->cancelDrag(point);
        break;
    }
  }

  if (m_clickin) {
    if (m_pActiveTool != NULL)
      m_pActiveTool->click(point);
    m_clickin = false;
  }

  updateMouseCursor(point);
}

void Editor::keyPressed(const KeyEvent& keyEvent) {
  if (m_pActiveTool != NULL)
    m_pActiveTool->keyPressed(keyEvent);
}

void Editor::keyReleased(const KeyEvent& keyEvent) {
  if (m_pActiveTool != NULL)
    m_pActiveTool->keyReleased(keyEvent);
}

} // end of markup