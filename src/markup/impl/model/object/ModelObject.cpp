#include "precomp.h"
#include "markup/impl/model/object/ModelObject.h"

namespace markup {

using namespace std;

void ModelObject::notifyObjectUpdated() {
  if (m_pListener != NULL)
    m_pListener->objectUpdated(ModelObjectEvent(this));
}

}; // end of markup