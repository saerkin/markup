#include "precomp.h"
#include "PolygonObject.h"

namespace markup {

ModelObject* PolygonObject::clone() const {
  return new PolygonObject(*this);
}

void PolygonObject::setPolygon(const Polygon& polygon) {
  if (this->polygon != polygon) {
    this->polygon = polygon;
    notifyObjectUpdated();
  }
}

void PolygonObject::move(double dx, double dy) {
  this->polygon.move(dx, dy);
  notifyObjectUpdated();
}

} // end of markup

