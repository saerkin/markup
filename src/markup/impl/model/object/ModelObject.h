#pragma once

#include "markup/model.h"

namespace markup {

class ModelObject;
struct IModelObjectListener;
class ModelObjectEvent;

/**
 * Базовый класс реализаций элемента модели
 */
class ModelObject : virtual public IModelObject {
// Fields
private:
  axiom::cstring m_name;

  int m_status;

  /**
   * Актуальное значение охватывающего прямоугольника.
   * Его следует вовремя обновлять в потомках класса с помощью метода setBoundingBox()
   */
  Box m_boundingBox;

  IModelObjectListener* m_pListener;

// Constructors/destructor
protected:
  inline explicit ModelObject(const axiom::cstring& name) :
    m_name(name),
    m_status(0),
    m_pListener(NULL) {}

  inline ModelObject(const ModelObject& o) :
    m_name(o.m_name),
    m_status(o.m_status),
    m_boundingBox(o.m_boundingBox),
    m_pListener(NULL) {}

public:
  virtual ~ModelObject() {}

// Attributes
public:
  const axiom::cstring& name() const {
    return m_name;
  }

//  inline void setName(const axiom::cstring& name) {
//    m_name = name;
//  }

  int status() const {
    return m_status;
  }

  inline void setStatus(int status) {
    m_status = status;
  }

  const Box& getBoundingBox() const {
    return m_boundingBox;
  }

  virtual void move(double dx, double dy) = 0;

//  inline const IModelObjectListener* getListener() const {
//    return m_pListener;
//  }

  inline void setListener(IModelObjectListener* pListener) {
    m_pListener = pListener;
  }

// Operators
private:
  ModelObject& operator=(const ModelObject& o);

// Operations
public:
  /**
   * Возвращает копию объекта
   */
  virtual ModelObject* clone() const = 0;

protected:
  void notifyObjectUpdated();
};

/**
 * Слушатель событий элемента модели
 */
struct IModelObjectListener : virtual public IEventListener {
  virtual void objectUpdated(const ModelObjectEvent& objectEvent) = 0;

protected:
  virtual ~IModelObjectListener() {}
};

/**
 * Событие в элементе разметки
 */
class ModelObjectEvent : public Event {
// Fields
private:
  IModelObject* m_pObject;

// Constructors/destructor
public:
  explicit ModelObjectEvent(IModelObject* pObject)
    : m_pObject(pObject) {}

// Methods
public:
  IModelObject* getObject() {
    return m_pObject;
  }
};

} // end of markup