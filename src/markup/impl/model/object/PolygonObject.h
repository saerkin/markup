#pragma once

#include "markup/model.h"
#include "markup/impl/model/object/ModelObject.h"

namespace markup {

/**
 * 
 */
class PolygonObject : public ModelObject, public IPolygonObject {
// Fields
private:
  Polygon polygon;

// Constructors/destructor
public:
  inline explicit PolygonObject(const std::string& name)
    : ModelObject(name) {}

private:
  PolygonObject(const PolygonObject& o)
    : ModelObject(o), polygon(o.polygon) {}

// Methods
public:
  ModelObjectType type() const { return MOT_POLYGON; }

  const Polygon& getPolygon() const {
    return polygon;
  }

  void setPolygon(const Polygon& polygon);

  void move(double dx, double dy);

  ModelObject* clone() const;
};

} // end of markup