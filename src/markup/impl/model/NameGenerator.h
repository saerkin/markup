#pragma once

#include <axiom/lifescope.h>
#include <axiom/cstring.h>
#include "markup/model.h"

#include <stdexcept>

namespace markup {

/**
 *
 */
class NameGenerator : axiom::LifeScopeObject {
// Fields
private:
  int m_polygonCounter;

// Constructors
public:
  inline explicit NameGenerator(axiom::LifeScope& lifeScope) :
    axiom::LifeScopeObject(lifeScope),
    m_polygonCounter(0) {}

private:
  NameGenerator(const NameGenerator& other);
  NameGenerator& operator=(const NameGenerator& other);

  inline axiom::cstring nextObjectName(ModelObjectType objectType) {
    using namespace axiom;
    switch (objectType) {
      case MOT_POLYGON:
        return format("poly%04d", ++m_polygonCounter);
      default:
        throw std::invalid_argument(
          format("Unexpected object type %s", objectType)
        );
    }
  }
};

} // end of markup