#include "precomp.h"
#include "markup/graphics.h"

namespace markup {

using namespace axiom;

const Color Color::TRANSPARENT(-1, -1, -1);
const Color Color::BLACK(0, 0, 0);
const Color Color::WHITE(255, 255, 255);
const Color Color::GREEN(0, 255, 0);

const Font Font::DEFAULT("Arial", 20);

} // end of markup
