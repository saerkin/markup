#include "precomp.h"
#include "markup/impl/window/WinAPIWindow.h"

namespace markup {

using namespace axiom;

static inline LONG round(const double& d) {
  return d >= 0 ? (LONG) (d + 1) : (LONG) (d - 1);
}

/**
 * 
 */
class WinAPIPoint : public POINT {
// Constructors/destructor
public:
  inline WinAPIPoint(Point& point) { // NOLINT(google-explicit-constructor)
    x = round(point.x);
    y = round(point.y);
  }

private:
  WinAPIPoint(const WinAPIPoint&);

// Operators
public:
  inline operator Point() const { // NOLINT(google-explicit-constructor)
    return Point(x, y);
  }

private:
  WinAPIPoint& operator=(const WinAPIPoint&);
};

/**
 * 
 */
class WindowDC {
// Fields
private:
  mutable HWND m_hwnd;
  mutable HDC m_hdc;

// Constructors/destructor
public:
  inline WindowDC(HWND hwnd) : m_hwnd(hwnd) { // NOLINT(google-explicit-constructor)
    m_hdc = ::GetDC(hwnd);
  }

  inline ~WindowDC() {
    ::ReleaseDC(m_hwnd, m_hdc);
  }

private:
  WindowDC(const WindowDC&);

// Operators
public:
  inline operator HDC() const { return m_hdc; } // NOLINT(google-explicit-constructor)

private:
  WindowDC& operator=(const WindowDC&);

// Methods
public:
  inline void drawFocusRect(const Rectangle& rect) {
    RECT rc = {
      round(rect.left), round(rect.top),
      round(rect.right), round(rect.bottom)
    };
    ::DrawFocusRect(m_hdc, &rc);
  }
};

//////////////////////////////////////////////////////////////////////////
/**
 * WinAPIWindow
 */

WinAPIWindow::WinAPIWindow() {
  m_hwnd = NULL;
  m_hDefaultCursor = NULL;
  m_focusBoxShowed = false;
}

bool WinAPIWindow::detectDrag(const Point& point) {
  assert(m_hwnd);
  Point screenPoint = m_markup2screen * point;
  return FALSE != ::DragDetect(m_hwnd, WinAPIPoint(screenPoint));
}

void WinAPIWindow::captureMouse() {
  assert(m_hwnd);
  ::SetCapture(m_hwnd);
}

void WinAPIWindow::releaseMouse() {
  assert(m_hwnd);
  ::ReleaseCapture();
}

void WinAPIWindow::setMouseCursor(MouseCursor cursor) {
  assert(m_hwnd);

  HCURSOR hCursor = NULL;
  switch (cursor) {
    case MOUSECURSOR_DEFAULT:
      hCursor = m_hDefaultCursor;
      break;
    case MOUSECURSOR_MOVE:
      hCursor = ::LoadCursor(NULL, IDC_SIZEALL);
      break;
    case MOUSECURSOR_RESIZE_NESW:
      hCursor = ::LoadCursor(NULL, IDC_SIZENESW);
      break;
    case MOUSECURSOR_RESIZE_NS:
      hCursor = ::LoadCursor(NULL, IDC_SIZENS);
      break;
    case MOUSECURSOR_RESIZE_NWSE:
      hCursor = ::LoadCursor(NULL, IDC_SIZENWSE);
      break;
    case MOUSECURSOR_RESIZE_WE:
      hCursor = ::LoadCursor(NULL, IDC_SIZEWE);
      break;
    default:
      throw std::runtime_error(
        format("Unknown mouse cursor %d", cursor)
      );
  }

  if (hCursor != NULL) {
    if (hCursor != (HCURSOR) ::GetClassLong(m_hwnd, GCL_HCURSOR)) {
      ::SetClassLong(m_hwnd, GCL_HCURSOR, (LONG) hCursor);
    }
  }
}

bool WinAPIWindow::isKeyPressed(int key) {
  switch (key) {
    case InputEvent::ALT:
      return ::GetKeyState(VK_MENU) < 0;
    case InputEvent::CTRL:
      return ::GetKeyState(VK_CONTROL) < 0;
    case InputEvent::SHIFT:
      return ::GetKeyState(VK_SHIFT) < 0;
    default:
      return false;
  }
}

void WinAPIWindow::showFocusBox(bool show) {
  if (show == m_focusBoxShowed) {
    return;
  }

  WindowDC dc(m_hwnd);
  prepareDC(dc);
  dc.drawFocusRect(m_focusBox);

  m_focusBoxShowed = show;
}

void WinAPIWindow::invalidate() {
  ::InvalidateRect(m_hwnd, NULL, TRUE);
}

LRESULT WinAPIWindow::handleWindowMessage(UINT message, WPARAM wParam, LPARAM lParam) {
  return DefWindowProc(m_hwnd, message, wParam, lParam);
}

void WinAPIWindow::prepareDC(HDC hdc) { // NOLINT(readability-make-member-function-const)
  ::SetMapMode(hdc, MM_TEXT);
  ::SetWindowOrgEx(
    hdc,
    round(m_markup2screen.offset.x),
    round(m_markup2screen.offset.y),
    NULL
  );
}

} // end of markup