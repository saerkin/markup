#pragma once

#pragma warning (disable:4250) // inherits via dominance

#include <axiom/lifescope.h>
#include <axiom/cstring.h>
#include <axiom/minmax.h>
#include <axiom/array.h>
#include <axiom/set.h>

#include <cassert>

#include <windows.h>